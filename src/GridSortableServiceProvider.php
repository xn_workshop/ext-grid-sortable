<?php

namespace Xn\GridSortable;

use Illuminate\Support\ServiceProvider;
use Xn\Admin\Facades\Admin;

class GridSortableServiceProvider extends ServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function boot(GridSortable $extension)
    {
        if (!GridSortable::boot()) {
            return;
        }

        if ($this->app->runningInConsole() && $assets = $extension->assets()) {
            $this->publishes(
                [$assets => public_path('vendor/grid-sortable')],
                'grid-sortable'
            );
        }

        GridSortable::routes(__DIR__.'/../routes/web.php');

        Admin::booting(function () {
            Admin::headerJs('vendor/grid-sortable/jquery-ui.min.js');
        });

        $extension->install();
    }
}
