<?php

namespace Xn\GridSortable\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class GridSortableController extends Controller
{
    public function sort(Request $request)
    {
        $sorts = $request->get('_sort');
        $modelPath = $request->get('_model');
        $modelClass = new $modelPath;


        $orders = collect($sorts)->pluck('sort');
        if (isset($modelClass->sortable['direction']) && strtolower($modelClass->sortable['direction']) == 'desc') {
            $orders = $orders->sortDesc();
        }else{
            $orders = $orders->sort();
        }

        $sorts = collect($sorts)
            ->pluck('key')
            ->combine($orders);

        $status     = true;
        $message    = trans('admin.save_succeeded');

        try {
            /** @var \Illuminate\Database\Eloquent\Collection $models */
            $models = $modelClass->find($sorts->keys());

            foreach ($models as $model) {
                $column = data_get($model->sortable, 'order_column_name', 'order_column');

                $model->{$column} = $sorts->get($model->getKey());
                $model->save();
            }
        } catch (Exception $exception) {
            $status  = false;
            $message = $exception->getMessage();
        }

        return response()->json(compact('status', 'message'));
    }
}
