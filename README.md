Laravel-Admin - grid-sortable
======

## Installation

```shell
composer require xn/grid-sortable
```

## Usage

Define your model

```php
<?php

use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Xn\GridSortable\Traits\SortableTrait;

class MyModel extends Model implements Sortable
{
    use SortableTrait;

    public $sortable = [
        'order_column_name' => 'order_column',
        'sort_when_creating' => true,
        'direction' => 'desc',
    ];
}
```

Use in grid

```php
$grid = new Grid(new MyModel);

$grid->sortable();
```


## Translation

The default text for the button is `Save order`. If you use an other language, such as Simplified Chinese, you can add a translation to the `resources/lang/zh-CN.json` file.

```json
{
    "Save order": "保存排序"
}
```

License
------------
Licensed under [The MIT License (MIT)](LICENSE).
